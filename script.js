// console.log("Hello World!");

// [SECTION] - Objects
/*SYNTAX: let/const objectName = {
	keyA: valueA,
	keyB: valueB
}*/

let cellphone = {
	name: "Nokia 3210",
	manufactureDate: 1999
};

console.log("Result from creating objects using initializers/literal notation");
console.log(cellphone);
console.log(typeof cellphone);

// This is an object constructor function.
//It allows us to create an object with a defined structure and fields.

function Laptop(name, manufactureDate){
	this.name = name
	this.manufactureDate = manufactureDate
}

// This is an instance of an object
let laptop = new Laptop("Lenovo", 2008);
console.log("Result from creating object using object constructors: ");
console.log(laptop);

let myLaptop = new Laptop("Macbook Air", 2020);
console.log("Result from creating object using object constructors: ");
console.log(myLaptop);

// we cannot create new object without the instace or the "new" keyword
let oldLaptop = Laptop("Portal R2E CCMC", 1980);
console.log("Result from creating object using object constructors: ");
console.log(oldLaptop); //return undefined value

// Creating empty onjects
let computer = {}
console.log(computer);

let array = [laptop, myLaptop];

// this is confusing because of the array method
console.log(array[0]["name"]);

// proper way of accesing object property value is by using dot notation
console.log(array[0].name);

// [SECTION] - Initializing, Deleting, Re-assigning Object Properties

let car = {};

// Adding new key and value
car.name = "Honda Civic";
console.log(car);

//We can create objects with:

//{} - Object Literal - Great for creating dynamic objects
let pokemon1 = {
	name: "Pikachu",
	type: "Electric"
}
let pokemon10 = {
	name: "Mew",
	type: ["Psychic","Normal"]
}
let pokemon25 = {
	name: "Charizard",
	level: 30
}
//Constructor function - allows us to create objects with a defined structure

function Pokemon(name,type,level){

	//this - keyword used to refer to the object to be created with the constructor function
	this.name = name;
	this.type = type;
	this.level = level;
}

//new keyword allows us to create an instance of an object with a constructor function.

let pokemonInstance1 = new Pokemon("Bulbasaur","Grass",32);

//Objects can have arrays as properties:

let professor1 = {
	name: "Romenick Garcia",
	age: 25,
	subjects: ["MongoDB","HTML","CSS","JS"]
}

//We can access object properties with dot notation
console.log(professor1.subjects);

//What if want to add an item in the array within the object?
//What if we want to add "NodeJS" as subject for professor1?
professor1.subjects.push("NodeJS");

//Mini-Activity: Display in the console each subject from professor1's subjects array.
//Take a screenshot of your output and send it in our hangouts

//Approach 1 - access the items from the array using its index:
/*console.log(professor1.subjects[0]);
console.log(professor1.subjects[1]);
console.log(professor1.subjects[2]);
console.log(professor1.subjects[3]);
console.log(professor1.subjects[4]);*/

//Approach 2 - we can also use forEach() to display each item in the array:
professor1.subjects.forEach(function(subject){
	console.log(subject);
})

let dog1 = {
	name: "Bantay",
	breed: "Golden Retriever"
};
let dog2 = {
	name: "Bolt",
	breed: "Aspin"
};

//Array of objects:
let dogs = [dog1,dog2];

//How can we display the details of the first item in the dogs array?
//You can use the index number of the item to access it from the array:
console.log(dogs[0])
//How can we display the value of the name of the property of the second item?
console.log(dogs[1].name);
console.log(dogs[1]["name"]);

//What if we want to delete the second item from the dogs array?
dogs.pop();

dogs.push({

	name: "Whitey",
	breed: "Shih Tzu"

})

// Initialize, delete, update object properties
let supercar1 = {};
console.log(supercar1);

// Initialize properties and values with our empty object:
supercar1.brand = "Porsche";
console.log(supercar1);
supercar1.model = "Porsche 911";
supercar1.price = 182900;
console.log(supercar1);

// Delete object properties with the "delete" keyword
delete supercar1.price;
console.log(supercar1);

// Update the values of an object using the dot notation:
supercar1.model = "718 Boxster";
console.log(supercar1);

pokemonInstance1.type = "Grass, Normal";
console.log(pokemonInstance1);

// Object Methods
// Function in an object
// This is useful for creating functions that re associated to specific object

let person1 = {
	name: "Joji",
	talk: function(){
		console.log("Hello!");
	}
};
person1.talk();
// talk();

let person2 = {
	name: "Joe",
	talk: function(){
		console.log("Hello, World!");
	}
};
person2.talk();
person2.walk = function(){
	console.log("Joe has walked 500 miles just to be the man that walked 500 miles to be at your door.");
}
person2.walk();

person1.introduction = function(){
	console.log("Hi! I am " + this.name + "!");
}
person1.introduction();

person2.introduction = function(){
	console.log(this);
}
person2.introduction();

// Mini-Activity
// Create a new object as student1
// The object should have the following properties:
// name, age, address
// Add 3 methods for student1
// introduceName = which will display the student1's name as:
// "Hello! My name is <nameofstudent1>"
// introduceAge = which will display the age of student1 as:
// "Hello! I am <ageofstudent1> years old."
// introduceAddress = which will display the address of studen1 as:
// "Hello! I am <nameofstudent1>. I live in <addressofstudent1>"
// Invoke all 3 methods and send your output in the hangouts.

let student1 = {
	name: "Jeff",
	age: 25,
	address: "The Darkside, Moon",
	introduceName: function(){
		console.log("Hello! My name is " + this.name);
	},
	introduceAge: function(){
		console.log("Hello! I am " + this.name + " years old.");
	},
	introduceAddress: function(){
		console.log("Hello! I am " + this.name + ". I live in " + this.address);
	}
}

student1.introduceName();
student1.introduceAge();
student1.introduceAddress();

function Student(name, age, address){
	this.name = name;
	this.age = age;
	this.address = address;

	this.introduceName = function(){
		console.log("Hello! My name is " + this.name);
	}
	this.introduceAge = function(){
		console.log("Hello! I am " + this.name + " years old.");
	}
	this.introduceAddress = function(){
		console.log("Hello! I am " + this.name + ". I live in " + this.address);
	}
	// We can also add methods that take other objects as an arguement
	this.greet = function(person){
		console.log("Good Day, " + person.name + "!");
	}
}
let student = new Student("Jeff", 25, "The Darkside, Moon");
console.log(student);

let student2 = new Student("Joe", 26, "Quezon");
student2.greet(student);

// Mini-Activity
// Create a new constructor function called Dog which is able to create objects with the following properties:
// name, breed
// The constructor function should also have 2 methods:
// call() - is a method whill allows us to display the following message:
// "Bark Bark Bark!"
// greet() - is a method which allows us to greet another person. This method should be able to receive an object which has a name property. Upon invoking the greet() method it should display:
// "Bark, Bark, <nameofperson>"

// Create an instance/object from the Dog constructor
// Invoke its call() method.
// Invoke its greet method to greet newStudent2.

// Take a screeshot of your output and send it to the hangouts.

function Dog(name, breed){
	this.name = name;
	this.breed = breed;

	this.call = function(){
		console.log("Bark, Bark, Bark!");
	}

	this.greet = function(person){
		console.log("Bark, Bark, " + person.name);
	}
}

let newDog1 = new Dog("John", "Rottweiler");

newDog1.call();
newDog1.greet(student2);
newDog1.greet(supercar1);

function sample1(sampleParameter){
	newDog1.name = "Gary";
	console.log(sampleParameter.name);
}

sample1(newDog1);
console.log(newDog1);
